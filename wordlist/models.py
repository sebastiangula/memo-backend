from django.db import models
from django.conf import settings
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

class Language(models.Model):
    name = models.CharField(max_length=50)
    language_code = models.CharField(max_length=5, primary_key = True)

class Wordlist(models.Model):
    name = models.CharField(max_length=50, validators=[RegexValidator(regex='^.{8,}$', message='Length has to be at least 8', code='nomatch')])
    description = models.CharField(max_length=600, blank=True, null=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_published = models.BooleanField(default=False)
    original_language = models.ForeignKey(Language, related_name="%(class)s_original_language", on_delete=models.CASCADE)
    translation_language = models.ForeignKey(Language, related_name="%(class)s_translation_language", on_delete=models.CASCADE)
    image = models.FileField(default='/default.png', blank=False, null=False)
    def save(self,*args,**kwargs):
        if self.original_language == self.translation_language:
            raise ValidationError('Languages cannot be equal')
        super().save(*args, **kwargs)

class WordlistEnrollment(models.Model):
    wordlist = models.ForeignKey(Wordlist, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    class Meta:
        unique_together = ('wordlist', 'user')