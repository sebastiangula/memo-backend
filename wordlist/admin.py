from django.contrib import admin
from wordlist.models import Language, Wordlist, WordlistEnrollment

@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    pass

@admin.register(Wordlist)
class WordlistAdmin(admin.ModelAdmin):
    pass

@admin.register(WordlistEnrollment)
class WordlistEnrollment(admin.ModelAdmin):
    pass