from rest_framework import serializers, status
from wordlist.models import Wordlist, Language, WordlistEnrollment
from words.models import Word
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework import mixins, generics
from rest_framework.parsers import MultiPartParser
from rest_framework.viewsets import ModelViewSet
from django.shortcuts import get_object_or_404
from django.db import IntegrityError
from django.db.models import Q

from django.http import Http404

class WordlistSerializer(serializers.ModelSerializer):
    owner_username = serializers.ReadOnlyField(source='owner.username')
    translation_language_name = serializers.ReadOnlyField(source='translation_language.name')
    original_language_name = serializers.ReadOnlyField(source='original_language.name')
    image = serializers.FileField(required=False)

    def validate(self, data):
        if 'original_language' in data and 'translation_language' in data and data['original_language'] == data['translation_language']:
            raise serializers.ValidationError("Languages cannot be equal")
        return data

    class Meta:
        model = Wordlist
        fields = ('id', 'name', 'description', 'original_language', 'translation_language', 'owner', 'owner_username', 'translation_language_name', 'original_language_name', 'image')
        read_only_fields = ('owner', )

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('name', 'language_code')

class WordlistView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WordlistSerializer

    def get_object(self):
        return get_object_or_404(Wordlist, pk = self.kwargs['wordlist_id'])

class NewWordlistView(generics.CreateAPIView):
    serializer_class = WordlistSerializer

    def perform_create(self, serializer):
        serializer.save(owner_id=self.request.user.id)

class WordlistImageUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wordlist
        fields = ('image', )

class WordlistImageUpload(mixins.UpdateModelMixin, generics.GenericAPIView):
    serializer_class = WordlistImageUploadSerializer
    parser_classes = (MultiPartParser,)

    def get_object(self):
        return get_object_or_404(Wordlist, pk = self.kwargs['wordlist_id'])

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

@api_view(['GET'])
def wordlists(request):
    if 'type' not in request.GET or request.GET['type'] == 'all':
        wordlists = Wordlist.objects.all()
    elif request.GET['type'] == 'enrolled':
        if 'user' in request.GET:
            ids = WordlistEnrollment.objects.filter(user__username = request.GET['user']).values_list('wordlist', flat=True)
            wordlists = Wordlist.objects.filter(Q(id__in = ids) | Q(owner__username = request.GET['user']))
        else:
            ids = WordlistEnrollment.objects.filter(user = request.user.id).values_list('wordlist', flat=True)
            wordlists = Wordlist.objects.filter(Q(id__in = ids) | Q(owner = request.user.id))
    elif request.GET['type'] == 'created':
        if 'user' in request.GET:
            wordlists = Wordlist.objects.filter(owner__username = request.GET['user'])
        else:
            wordlists = Wordlist.objects.filter(owner = request.user.id)
    if 'first' in request.GET and request.GET['first'] != 'null':
        wordlists = wordlists.filter(original_language = request.GET['first'])
    if 'second' in request.GET and request.GET['second'] != 'null':
        wordlists = wordlists.filter(original_language = request.GET['second'])
    serializer = WordlistSerializer(wordlists, many=True)
    return JsonResponse(serializer.data, safe=False)

@api_view(['GET'])
def languages(request):
    if request.method == 'GET':
        languages = Language.objects.all()
        serializer = LanguageSerializer(languages, many=True)
        return JsonResponse(serializer.data, safe=False)

class WordlistEnrollmentView(mixins.DestroyModelMixin, generics.GenericAPIView):
    model = WordlistEnrollment
    queryset = WordlistEnrollment.objects.all()

    def get_object(self):
        try:
            return self.queryset.get(user__id = self.request.user.id, wordlist__id = self.kwargs['wordlist_id'])
        except WordlistEnrollment.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        try:
            enrollment = self.get_object()
        except Http404:
            return JsonResponse({'enrolled': False}, status=status.HTTP_200_OK)
        return JsonResponse({'enrolled': True}, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        enrollment = WordlistEnrollment(user_id = request.user.id, wordlist_id = self.kwargs['wordlist_id'])
        try:
            enrollment.save()
        except IntegrityError as e:
            if 'UNIQUE constraint' in e.args[0]:
                return JsonResponse({'details': 'User is already enrolled to this course.'}, status=status.HTTP_403_FORBIDDEN)
            else:
                raise IntegrityError(e)
        return JsonResponse({}, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)