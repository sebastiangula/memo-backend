from django.shortcuts import render
from wordlist.models import Wordlist
from words.models import Word, WordStatus
from rest_framework.decorators import api_view
from rest_framework import serializers, status
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from django.db.models import F
from django.utils.timezone import now
import datetime
import json

class CustomWordSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        original_language = obj.wordlist.original_language.name
        translation_language = obj.wordlist.translation_language.name
        return {
            'id': obj.id,
            'pair': [
                {'word': obj.original_word, 'language': original_language},
                {'word': obj.translation_word, 'language': translation_language}
            ],
            'status': obj.status
        }

class WordlistGame(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        words = Word.objects.with_status(kwargs['wordlist_id'], request.user.id)
        amount = request.GET['amount'] if 'amount' in request.GET else 10
        words = words[:amount]
        serializer = CustomWordSerializer(words, many=True)
        return JsonResponse(serializer.data, safe=False)

    def patch(self, request, *args, **kwargs):
        score = int(request.data['score'])
        default_status = 1 if score == 1 else 0
        word_status, created = WordStatus.objects.get_or_create(user_id = request.user.id, word_id = int(request.data['word_id']), defaults = {'status': default_status})
        if created == False:
            if word_status.status == 0 and score == 1:
                word_status.status = 1
                word_status.save()
            elif word_status.status == 1 and word_status.modified_date <= now() - datetime.timedelta(minutes=15) and score == '1':
                word_status.status = 2
                word_status.save()
            elif word_status.status == 2 and word_status.modified_date <= now() - datetime.timedelta(days=1):
                if score == 1:
                    word_status.status = 3
                    word_status.save()
                else:
                    word_status.status = 1
                    word_status.save()
            elif word_status.status == 3 and word_status.modified_date <= now() - datetime.timedelta(days=1):
                word_status.status = 2
                word_status.save()
            else:
                return JsonResponse({"status": word_status.status}, status=status.HTTP_200_OK)
        return JsonResponse({"status": word_status.status}, status=status.HTTP_201_CREATED)