"""memo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from wordlist.views import wordlists, languages, NewWordlistView, WordlistView, WordlistImageUpload, WordlistEnrollmentView
from games.views import WordlistGame
from words.views import WordView, words, words_with_status
from accounts.views import register
from common.views import csrf
from rest_framework_jwt.views import refresh_jwt_token
from django.conf.urls.static import static
from django.conf import settings
from accounts.views import ProfileView, ProfileImageUpload, update_account, UserProfileView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^words_with_status/(?P<wordlist_id>[0-9]+)', words_with_status),
    url(r'^wordlists/', wordlists),
    url(r'^wordlist/(?P<wordlist_id>[0-9]+)/enrollment', WordlistEnrollmentView.as_view()),
    url(r'^wordlist/(?P<wordlist_id>[0-9]+)/image/upload', WordlistImageUpload.as_view()),
    url(r'^wordlist/(?P<wordlist_id>[0-9]+)', WordlistView.as_view()),
    url(r'^wordlist/new', NewWordlistView.as_view()),
    url(r'^words/(?P<wordlist_id>[0-9]+)', words),
    url(r'^word/(?P<word_id>[0-9]+)', WordView.as_view()),
    url(r'^word', WordView.as_view()),
    url(r'^register/', register),
    url(r'^csrf/', csrf),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^languages/', languages),
    url(r'^game/words/(?P<wordlist_id>[0-9]+)', WordlistGame.as_view()),
    url(r'^my_profile/', ProfileView.as_view()),
    url(r'^profile/(?P<username>[\w.@+-]+)', UserProfileView.as_view()),
    url(r'^profile_image_upload/', ProfileImageUpload.as_view()),
    url(r'^update_account', update_account)
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)