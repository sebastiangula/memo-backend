from django.contrib import admin
from words.models import Word, WordStatus

@admin.register(WordStatus)
class WordStatus(admin.ModelAdmin):
    pass

@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    pass