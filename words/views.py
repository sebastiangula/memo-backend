from rest_framework import serializers, status
from words.models import Word
from wordlist.models import Wordlist
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view

class WordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = ('id', 'original_word', 'translation_word', 'wordlist')

class WordWithStatusSerializer(serializers.ModelSerializer):
    status = serializers.IntegerField()

    class Meta:
        model = Word
        fields = ('id', 'original_word', 'translation_word', 'status')

class WordView(APIView):
    def post(self, request):
        serializer = WordSerializer(data=request.data)
        if serializer.is_valid():
            word = serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED, safe=None)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, word_id):
        word = get_object_or_404(Word, pk=word_id)
        serializer = WordSerializer(word, data=request.data, partial=True)
        if serializer.is_valid():
            word = serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED, safe=None)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, word_id):
        word = get_object_or_404(Word, pk=word_id)
        word.delete()
        return HttpResponse(status=status.HTTP_202_ACCEPTED)

@api_view(['GET'])
def words(request, wordlist_id):
    word_list = Word.objects.filter(wordlist__pk=wordlist_id)
    serializer = WordSerializer(word_list, many=True)
    return JsonResponse(serializer.data, safe=False)

@api_view(['GET'])
def words_with_status(request, wordlist_id):
    words = Word.objects.with_status(wordlist_id, request.user.id)
    serializer = WordWithStatusSerializer(words, many=True)
    return JsonResponse(serializer.data, safe=False)