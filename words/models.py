from django.db import models
from django.contrib.auth.models import User
from wordlist.models import Wordlist
from django.conf import settings

class WordManager(models.Manager):
    def with_status(self, wordlist, user):
        from django.db import connection
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT "word"."id", "word"."wordlist_id", "word"."original_word", "word"."translation_word", "status"."status" FROM "words_word" as "word" LEFT JOIN
                (SELECT * FROM "words_wordstatus" WHERE "words_wordstatus"."user_id" = %d) as "status" ON "status"."word_id" = "word"."id"
                WHERE "word"."wordlist_id" = %d
            """ % (int(user), int(wordlist)))
            result_list = []
            for row in cursor.fetchall():
                p = self.model(id=row[0], wordlist_id=row[1], original_word=row[2], translation_word=row[3])
                if row[4] is None:
                    p.status = 0
                else:
                    p.status = row[4]
                result_list.append(p)
            return result_list

class Word(models.Model):
    wordlist = models.ForeignKey(Wordlist, on_delete=models.CASCADE)
    original_word = models.CharField(max_length=50)
    translation_word = models.CharField(max_length=50)
    objects = WordManager()

class WordStatus(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    status = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('word', 'user')

