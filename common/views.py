from django.shortcuts import render
from django.http import HttpResponse
from django.middleware.csrf import get_token

def csrf(request):
    if request.method == 'GET':
        return HttpResponse(get_token(request))
