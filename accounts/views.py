from django.shortcuts import render
from rest_framework import serializers, status
from rest_framework.validators import UniqueValidator
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from accounts.models import Profile
from rest_framework import mixins, generics
from rest_framework.parsers import MultiPartParser
from django.shortcuts import get_object_or_404
from django.contrib.auth.password_validation import validate_password
from django.db.models import Q


class ProfileSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(source='user.date_joined', read_only=True)
    class Meta:
        model = Profile
        fields = ('description', 'image', 'date_joined')

class ProfileView(generics.RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer

    def get_object(self):
        profile, _ = Profile.objects.get_or_create(user_id = self.request.user.id)
        return profile

class UserProfileView(generics.RetrieveAPIView):
    serializer_class = ProfileSerializer

    def get_object(self, *args, **kwargs):
        user = get_object_or_404(User, username=self.kwargs['username'])
        profile = get_object_or_404(Profile, Q(user_id = user.id))
        return profile

class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(max_length=30)
    last_name = serializers.CharField(max_length=30)
    email = serializers.EmailField(
            required = True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    username = serializers.CharField(
            validators=[UniqueValidator(queryset=User.objects.all()),
                RegexValidator(
                    regex='^[\w_]+$',
                    message='The username field may contain alpha-numeric characters as well as dashes and underscores',
                    code='invalid_username'
                )
            ],
            required = True,
            min_length = 4
            )
    password = serializers.CharField(min_length=8)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'password')

@api_view(['POST'])
@permission_classes(tuple())
def register(request):
    serialized = UserSerializer(data=request.data)
    if serialized.is_valid():
        User.objects.create_user(
            email=serialized.validated_data['email'],
            username=serialized.validated_data['username'],
            password=serialized.validated_data['password'],
            first_name = serialized.validated_data['first_name'],
            last_name = serialized.validated_data['last_name']
        )
        return JsonResponse(serialized.data, status=201)
    else:
        return JsonResponse(serialized.errors, status=400)

class ProfileImageUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('image', )

class ProfileImageUpload(mixins.UpdateModelMixin, generics.GenericAPIView):
    serializer_class = ProfileImageUploadSerializer
    parser_classes = (MultiPartParser,)

    def get_object(self):
        return get_object_or_404(Profile, user_id=self.request.user.id)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

class UpdateAccountSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required = False,
        validators=[UniqueValidator(queryset=User.objects.all())]
        )
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(min_length=8, required=False)

@api_view(['PATCH'])
def update_account(request):
    serializer = UpdateAccountSerializer(data=request.data)
    if serializer.is_valid():
        if not request.user.check_password(serializer.data.get('old_password')):
            return JsonResponse({'old_password': ['Wrong password.']}, 
                status=status.HTTP_400_BAD_REQUEST)
        user = request.user
        if 'new_password' in serializer.data:
            user.set_password(serializer.data['new_password'])
        if 'email' in serializer.data:
            user.email = serializer.data['email']
        user.save()
        return JsonResponse({}, 
                status=status.HTTP_201_CREATED)
    else:
        return JsonResponse(serializer.errors, 
            status=status.HTTP_400_BAD_REQUEST)