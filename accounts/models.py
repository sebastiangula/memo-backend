from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(max_length=250, blank=True, null=True)
    image = models.FileField(default='/default_avatar.png', blank=False, null=False)